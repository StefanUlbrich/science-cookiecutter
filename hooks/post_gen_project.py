# pylint: skip-file
import os
import shutil

print(os.getcwd())  # prints /absolute/path/to/{{cookiecutter.project_slug}}
# https://github.com/cookiecutter/cookiecutter/issues/723#issuecomment-350561930

def remove(filepath):
    if os.path.isfile(filepath):
        os.remove(filepath)
    elif os.path.isdir(filepath):
        shutil.rmtree(filepath)

current_dir = os.getcwd()


if not {{cookiecutter.use_sphinx}}: remove(os.path.join(current_dir,'doc'))
if not {{cookiecutter.use_pytest}}: remove(os.path.join(current_dir,'tests'))
if not {{cookiecutter.use_vscode}}: remove(os.path.join(current_dir,'.vscode'))

{% if cookiecutter.init_git == "True" %}
print("Initialize git")
os.system('git init ' + current_dir)
os.system('git config user.name '+'{{cookiecutter.author_name}}')
os.system('git config user.email '+'{{cookiecutter.author_email}}')
os.system('git add * && git add .*')
{% endif %}

{% if cookiecutter.init_vm == "True" %}
print({{cookiecutter.init_vm}})
os.system('poetry install')
os.system('sh scripts/install_kernel.sh')
os.system('git add poetry.lock')
{% endif %}

{% if cookiecutter.init_git == "True" %}
os.system('git commit -m"Initial commit"')
{% endif %}