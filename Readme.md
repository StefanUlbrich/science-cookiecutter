# Welcome to `bs-science-cookiecutter`

A cookie cutter for python packages just after Stefan's taste!
It uses 

* Poetry
* Sphinx (nbsphinx, napoleon, intersphinx for some of the most important science packages)
* pytest


First, [install  cookiecutter](https://cookiecutter.readthedocs.io/en/1.7.2/installation.html), then run

```sh
cookiecutter git@bitbucket.org:StefanUlbrich/science-cookiecutter.git
```
