#!/bin/sh
poetry run python -m ipykernel install --user --name {{cookiecutter.project_slug}} --display-name "Python 3 {{cookiecutter.project_name}}"