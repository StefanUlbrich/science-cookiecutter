# Welcome to {{cookiecutter.project_name}}

A scientific python package. This project has been created with my [science cookiecutter](https://bitbucket.org/StefanUlbrich/science-cookiecutter/src/master/).

## Installation and development

First make sure to install Python ({{cookiecutter.python_version}}) the dependency management
tool [Poetry](https://python-poetry.org/) then create an isolated virtual
environment and install the dependencies.

```sh
poetry install
```

Per terminal session,  the following command should be executed
to activate the virtual environment.

```sh
poetry shell
```

{% if cookiecutter.use_sphinx == "True" %}
To generate the documentation run:

```sh
cd doc/
make api # optional, only when the code base changes
make html
```
{% endif %}

{% if cookiecutter.use_pytest == "True" %}
To run unit tests, run:

```sh
pytest --log-level=WARNING
# Specify a selected test
pytest --log-level=DEBUG -k "TestExample"
pytest --log-level=DEBUG tests/test_example.py::TestExample::test_example
```
{% endif %}
{% if cookiecutter.use_vscode == "True" %}

To work with [VisualStudio Code](https://code.visualstudio.com/):

```sh
cp .vscode/template.settings.json .vscode/settings.json
which python # copy the path without the executable
```

and add the path to the virtual environment to in the `"python.pythonPath"` setting.

```sh
cp .vscode/template.settings.json .vscode/settings.json
which python # copy the path without the executable
```

and add the path to the virtual environment to in the `"python.pythonPath"` setting.
{% endif %}
