.. {{cookiecutter.project_name}} documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to {{cookiecutter.project_name}}
==============================

A super fancy science package, for instance for Kinematic Bézier Maps [ulbrich2011]_.
Don't forget to run :code:`make api`! User `intersphinx <https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html>`_: 
:class:`sklearn.base.BaseEstimator`.

Contents
^^^^^^^^

.. toctree::
   :maxdepth: 2

   about
   api/modules


Bibliography
^^^^^^^^^^^^

.. [ulbrich2011] : S. Ulbrich, V. Ruiz de Angulo, C. Torras, T. Asfour and R. Dillman. Kinematic Bézier maps. IEEE Transactions on Systems, Man and Cybernetics: Part B, 42(4): 1215-1230, 2012. 

Indices and tables
^^^^^^^^^^^^^^^^^^


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
